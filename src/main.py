from flask import Flask, jsonify, request, render_template
from flask_cors import CORS


# creating the Flask application
app = Flask(__name__, template_folder='maybeFront/gular/', static_url_path='/maybeFront/gular/' )
CORS = (app)


@app.route('/', methods=["GET", "POST"])
def main():
    return render_template('index.html')